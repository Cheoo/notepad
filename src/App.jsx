import { Layout } from 'antd';
const { Sider } = Layout;
import * as localForage from 'localforage';
import Header from './components/header/Header';
import Menu from './components/menu/Menu';
import Content from './components/content/Content';
import { redirect } from 'react-router-dom';

const App = () => {
  return (
    <Layout style={{ height: '100vh' }}>
      <Header />
      <Layout style={{ padding: '24px' }}>
        <Sider width={200}>
          <Menu />
        </Sider>
        <Layout>
          <Content />
        </Layout>
      </Layout>
    </Layout>
  );
};
export default App;

export const loaderApp = async () => {
  let items = [];

  await localForage.iterate((value, key) => {
    items.push({ id: key, ...value });
  });

  return items;
};

export const actionApp = async ({ request }) => {
  switch (request.method) {
    case 'POST': {
      const id = +new Date();
      await localForage.setItem(id.toString(), {
        label: 'Новая заметка',
        text: '',
      });
      return redirect(`/${id}/edit`);
    }
    case 'DELETE': {
      const formData = await request.formData();
      const id = formData.get('id');

      if (id) {
        await localForage.removeItem(id);

        const nextId = await localForage.keys();

        if (nextId.length) {
          return redirect(`/${nextId[0]}`);
        }

        return redirect('/');
      }
    }
  }
};
