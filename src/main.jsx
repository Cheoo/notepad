import React from 'react';
import ReactDOM from 'react-dom/client';
import 'antd/dist/reset.css';
import App, { loaderApp, actionApp } from './App';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Note, { loaderNote } from './components/note/Note';
import NoteEdit, { actionNoteEdit } from './components/noteEdidt/NoteEdit';
import { InitContent } from './components/initContent/InitContent';
import { ErrorPage } from './components/errorPage/ErrorPage';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,
    loader: loaderApp,
    action: actionApp,
    children: [
      {
        index: true,
        element: <InitContent />,
      },
      {
        path: ':id',
        element: <Note />,
        errorElement: <ErrorPage />,
        loader: loaderNote,
      },
      {
        path: ':id/edit',
        element: <NoteEdit />,
        errorElement: <ErrorPage />,
        loader: loaderNote,
        action: actionNoteEdit,
      },
      {
        path: '*',
        element: <ErrorPage />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router} />
);
