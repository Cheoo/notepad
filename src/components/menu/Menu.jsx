import { useLoaderData, useParams, Link } from 'react-router-dom';
import { Menu as MenuAside } from 'antd';
import { FileTextTwoTone } from '@ant-design/icons';

const Menu = () => {
  const notes = useLoaderData();
  const { id } = useParams();

  const items = notes?.map((item) => {
    return {
      key: item?.id,
      icon: <FileTextTwoTone />,
      label: item?.label,
      text: item?.text,
    };
  });

  const initMenu = [
    {
      label: 'Добавьте заметку',
      key: 'initMenu',
      disabled: true,
    },
  ];

  const menuItem = items ? (
    items.map((item) => (
      <MenuAside.Item key={item.key} icon={item.icon}>
        <Link to={`/${item.key}`}>{item.label}</Link>
      </MenuAside.Item>
    ))
  ) : (
    <MenuAside.Item>{initMenu.label}</MenuAside.Item>
  );

  return (
    <MenuAside
      mode='inline'
      defaultOpenKeys={[id]}
      selectedKeys={[id]}
      style={{
        height: '100%',
        borderRight: 0,
        paddingTop: 24,
      }}
    >
      {menuItem}
    </MenuAside>
  );
};

export default Menu;
