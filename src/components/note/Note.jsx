import * as localForage from 'localforage';
import { Typography, Button } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useNavigate, useLoaderData } from 'react-router-dom';
const { Title, Paragraph } = Typography;

const Note = () => {
  const { text, label } = useLoaderData();
  const nav = useNavigate();

  const initText = {
    label: 'Заголовок заметки',
    text: 'Текст заметки',
  };

  return (
    <>
      <div>
        <Title>{label ? label : initText.label}</Title>
        <Paragraph
          style={{
            width: '100%',
            height: '100%',
          }}
        >
          {text ? text : initText.text}
        </Paragraph>
      </div>
      <div
        style={{
          width: '100%',
        }}
      >
        <Button
          onClick={() => {
            nav('edit');
          }}
          type='primary'
          icon={<EditOutlined />}
          style={{
            width: '100%',
          }}
        >
          Редактировать
        </Button>
      </div>
    </>
  );
};

export default Note;

export const loaderNote = async ({ params }) => {
  const resp = await localForage.getItem(params.id);

  return resp;
};
