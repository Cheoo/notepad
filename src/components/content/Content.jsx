import { Layout, theme } from 'antd';
import { Outlet } from 'react-router-dom';

const Content = () => {
  const { Content } = Layout;
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <Content
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 24,
        margin: '0 0 0 24px',
        minHeight: 280,
        background: colorBgContainer,
      }}
    >
      <Outlet />
    </Content>
  );
};

export default Content;
