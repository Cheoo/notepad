import { Layout, Button, Input } from 'antd';
import { FileAddFilled, DeleteFilled } from '@ant-design/icons';
import { Form, useParams } from 'react-router-dom';

const Header = () => {
  const { Header } = Layout;
  const { id } = useParams();

  return (
    <Header
      className='header'
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <div
        className='logo'
        style={{
          fontWeight: 'bold',
          color: 'white',
        }}
      >
        NOTEPAD
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          width: '74px',
        }}
      >
        <Form method='delete'>
          <Input style={{ display: 'none' }} name='id' value={id} />
          <Button htmlType='submit' icon={<DeleteFilled />} shape='circle' />
        </Form>
        <Form method='post'>
          <Button
            htmlType='submit'
            type='dashed'
            shape='circle'
            icon={<FileAddFilled />}
          />
        </Form>
      </div>
    </Header>
  );
};

export default Header;
