import * as localForage from 'localforage';
import { Form, redirect, useLoaderData, useParams } from 'react-router-dom';
import { Button, Input } from 'antd';
import { SaveTwoTone } from '@ant-design/icons';
const { TextArea } = Input;

const NoteEdit = () => {
  const { label, text } = useLoaderData();
  const { id } = useParams();

  return (
    <Form
      method='post'
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: '100%',
      }}
    >
      <div>
        <Input style={{ display: 'none' }} name='id' value={id} />
        <Input placeholder='Title' name='label' defaultValue={label} />
        <TextArea
          placeholder='Text'
          name='text'
          defaultValue={text}
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: '1',
            margin: '24px 0',
            minHeight: 500,
          }}
        />
      </div>
      <Button htmlType='submit' type='primary' icon={<SaveTwoTone />}>
        Сохранить
      </Button>
    </Form>
  );
};

export default NoteEdit;

export const actionNoteEdit = async ({ request }) => {
  const formData = await request.formData();
  const id = formData.get('id');

  const submision = {
    text: formData.get('text'),
    label: formData.get('label'),
  };

  await localForage.setItem(id, submision);

  return redirect(`/${id}`);
};
